UPDATE 2017:
This project is on hold indefinitely. Not quite considering it abandon, but it may become obsolete before I get back to it. Currently, I am working on projects in Unity and Blender Game Engine, so this may end up getting adapted to one of those engines.

### License:
Copyright � 2015 Michal Kloko All Rights Reserved.

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that
it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License along
with this program. If not, see http://www.gnu.org/licenses/.

### NOTICE
This project is in very early stages of development. Commits often
represent extreme changes and core overhauls.

As it stands right now (just prior to writing this), I've split the
engine code from the scene code and refactored the engine to be more
general purpose. This will allow me to develop the engine separately and
use it in other projects. It is my goal that the engine will make a nice
springboard for others who would like to use the Panda3D engine, without
re-inventing a bunch of wheels (e.g. it will include a developer
console, a structured approach to engine variables/states, input and gui
helpers, and much more). I will be publishing that code in a new
repository later.

### Description:
A (soon-to-be) intuitive heightmap generator/editor written in Python,
using the Panda3D Engine. There are others out there, but I wanted the
experience. Maybe in time it will mature in to some stiff competition
for paid options, but for now, it is nothing more or less than a toy for
my own enjoyment.

I do apologize if the name is taken. A google search didn't yield
anything on the first page, that I noticed. Happy to change, if someone
raises a complaint!

### Requirements:
 - Panda3D Engine: https://www.panda3d.org/

### Usage:
It is not recommended to mess with this project at the current moment,
unless you're a developer and want to use part/all of the code in your
own project. For users, there are other tools out there that will
accomplish the task of generating heightmaps. One promising looking
example is: https://code.google.com/p/mudpie/, which I will be using as
refrence-material/inspiration for some parts of this project. Another
option, that also includes 3d previewing, is
http://www.bundysoft.com/L3DT/.
