# -*- coding: iso-8859-1 -*-

#~ PandaStart
#~ Copyright 2015 Michal Kloko All rights reserved.
#~ 
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~ 
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details. 
#~ 
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

""" data/CoreInput.py

Classes: CoreInput
"""

from panda3d.core import (
    CollisionTraverser, CollisionHandlerQueue, CollisionRay,
    CollisionNode, WindowProperties, Vec3, GeomNode
)

class CoreInput(object):
    """Inherits: None

    Description: Handles all mouse/keyboard input"""

    def __init__(self):
        ################################################################
        ### Base setup
        self.focus = Vec3(55,-55,20)
        self.heading = 180
        self.pitch = 0
        self.mousex = 0
        self.mousey = 0
        self.last = 0

        # disable panda's default mouse config
        base.disableMouse()

        # Hide the windows pointer/cursor
        self.winProps = WindowProperties()
        #self.winProps.setCursorHidden(True)
        base.win.requestProperties(self.winProps)

        # Start pointer at center
        base.win.movePointer(
            0, base.win.getXSize() / 2, base.win.getYSize() / 2
        )

        ################################################################
        ### Keyboard events
        self.keyState = [0] * len(self.keyCfg)

        base.accept(
            self.keyCfg['Fwd'],self.setKeyState,[0, 1]
        )
        base.accept(
            self.keyCfg['Fwd']+'-up',self.setKeyState,[0, 0]
        )

        base.accept(
            self.keyCfg['Rev'],self.setKeyState,[1, 1]
        )
        base.accept(
            self.keyCfg['Rev']+'-up',self.setKeyState,[1, 0]
        )

        base.accept(
            self.keyCfg['SlideL'],self.setKeyState,[2, 1]
        )
        base.accept(
            self.keyCfg['SlideL']+'-up',self.setKeyState,[2, 0]
        )

        base.accept(
            self.keyCfg['SlideR'],self.setKeyState,[3, 1]
        )
        base.accept(
            self.keyCfg['SlideR']+'-up',self.setKeyState,[3, 0]
        )

        base.accept(
            self.keyCfg['RollR'],self.setKeyState,[4, 1]
        )
        base.accept(
            self.keyCfg['RollR']+'-up',self.setKeyState,[4, 0]
        )

        base.accept(
            self.keyCfg['RollL'],self.setKeyState,[5, 1]
        )
        base.accept(
            self.keyCfg['RollL']+'-up',self.setKeyState,[5, 0]
        )

        base.accept(
            self.keyCfg['TurnL'],self.setKeyState,[6, 1]
        )
        base.accept(
            self.keyCfg['TurnL']+'-up',self.setKeyState,[6, 0]
        )

        base.accept(
            self.keyCfg['TurnR'],self.setKeyState,[7, 1]
        )
        base.accept(
            self.keyCfg['TurnR']+'-up',self.setKeyState,[7, 0]
        )

        base.accept(
            self.keyCfg['PitchUp'],self.setKeyState,[8, 1]
        )
        base.accept(
            self.keyCfg['PitchUp']+'-up',self.setKeyState,[8, 0]
        )

        base.accept(
            self.keyCfg['PitchDn'],self.setKeyState,[9, 1]
        )
        base.accept(
            self.keyCfg['PitchDn']+'-up',self.setKeyState,[9, 0]
        )

        base.accept(self.keyCfg['IncSpd'],self.incSpeed)
        base.accept(self.keyCfg['DecSpd'],self.decSpeed)
        base.accept(self.keyCfg['Exit'],self.stop, [True])
        base.accept(self.keyCfg['DevCon'],self.toggleDevCon)

        ################################################################
        ### Mouse picking
        base.accept('mouse1', self.mPick)

        # Collision Traverser
        self.mPickTraverser = CollisionTraverser()
        self.mCollisionQue = CollisionHandlerQueue()

        # Collision solid ray to detect against
        self.mPickRay = CollisionRay()
        self.mPickRay.setOrigin(base.camera.getPos(render))
        self.mPickRay.setDirection(render.getRelativeVector(
            camera, Vec3(0, 1, 0))
        )

        # Collison Node to hold the ray
        self.mPickNode = CollisionNode('mPickRay')
        self.mPickNode.addSolid(self.mPickRay)

        self.mPickNP = base.camera.attachNewNode(self.mPickNode)

        """Everything to be picked will use bit 1. This way if we were
        doing other collisions, we could seperate it. We use bitmasks to
        determine what to check other objects against; if they dont have
        a bitmask for bit 1, skip them!"""

        self.mPickNode.setFromCollideMask(
            GeomNode.getDefaultCollideMask()
        )

        # Register the ray as something that can cause collisions
        self.mPickTraverser.addCollider(
            self.mPickNP, self.mCollisionQue
        )

        # Show collisions for debugging
        #self.mPickTraverser.showCollisions(render)

    def setKeyState(self, key, state):
        self.keyState[key] = state

    def mPick(self):
        #do we have a mouse
        if (base.mouseWatcherNode.hasMouse() == False): return 0

        #get the mouse position
        mPos = base.mouseWatcherNode.getMouse()

        #Set the position of the ray based on the mouse position
        self.mPickRay.setFromLens(
            base.camNode, mPos.getX(), mPos.getY()
        )

        self.mPickTraverser.traverse(render)

        if (self.mCollisionQue.getNumEntries() > 0):
            self.mCollisionQue.sortEntries()
            entry = self.mCollisionQue.getEntry(0);
            pickedObj = entry.getIntoNodePath()

            pickedObj = pickedObj.findNetTag('mPickable')
            if not pickedObj.isEmpty():
                # Here is how you get the surface collsion
                #pos = entry.getSurfacePoint(render)
                # Now do stuff, e.g.:
                print pickedObj
                return pickedObj

    def incSpeed(self):
        self.i_basespeed *= 2
    def decSpeed(self):
        self.i_basespeed /= 2

    def handleInput(self, ctlStyle=1,task=None): # Input task
        """Arguments: None

        Description: Main input loop. Do not call directly."""
        if base.mouseWatcherNode.hasMouse():
            mX = base.mouseWatcherNode.getMouse().getX()
            mY = base.mouseWatcherNode.getMouse().getY()
            self.onDevHud['Mouse Pos'] = (mX, mY)

        if ctlStyle == 1: # Free-form controls
            if self.keyState[0]: # Move fwd/bwd
                base.camera.setY(base.camera, self.i_basespeed)
            elif self.keyState[1]:
                base.camera.setY(base.camera, -self.i_basespeed)
            if self.keyState[2]: # Strafe lft/rgt
                base.camera.setX(base.camera, -self.i_basespeed)
            elif self.keyState[3]:
                base.camera.setX(base.camera, self.i_basespeed)
            if self.keyState[4]: # Roll
                base.camera.setR(base.camera, self.i_rollspeed)
            elif self.keyState[5]:
                base.camera.setR(base.camera, -self.i_rollspeed)
            if self.keyState[6]: # Turn Left/Right
                base.camera.setH(base.camera, self.i_hpspeed)
            elif self.keyState[7]:
                base.camera.setH(base.camera, -self.i_hpspeed)

            if self.keyState[8]: # Pitch Up/Dn
                base.camera.setP(base.camera, self.i_hpspeed)
            elif self.keyState[9]:
                base.camera.setP(base.camera, -self.i_hpspeed)
        else: # FPS Controls - Modified from p3d Bump-Mapping sample
            md = base.win.getPointer(0)
            x = md.getX()
            y = md.getY()
            
            if base.win.movePointer(0, 100, 100):
                self.heading = self.heading - (x - 100) * 0.2
                self.pitch = self.pitch - (y - 100) * 0.2

            if (self.pitch < -45): self.pitch = -45
            if (self.pitch >  45): self.pitch =  45

            base.camera.setHpr(self.heading,self.pitch,0)
            direction = base.camera.getMat().getRow3(1)
            elapsed = task.time - self.last
            
            if (self.last == 0): elapsed = 0

            if self.keyState[0]:
                self.focus = self.focus + direction * elapsed*30
            if self.keyState[1]:
                self.focus = self.focus - direction * elapsed*30

            base.camera.setPos(self.focus - (direction*5))

            #~ if (base.camera.getX() < -59.0): base.camera.setX(-59)
            #~ if (base.camera.getX() >  59.0): base.camera.setX( 59)
            #~ if (base.camera.getY() < -59.0): base.camera.setY(-59)
            #~ if (base.camera.getY() >  59.0): base.camera.setY( 59)
            #~ if (base.camer a.getZ() <   5.0): base.camera.setZ(  5)
            #~ if (base.camera.getZ() >  45.0): base.camera.setZ( 45)

            self.focus = base.camera.getPos() + (direction*5)
            self.last = task.time

        return 1
