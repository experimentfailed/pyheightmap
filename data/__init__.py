 # -*- coding: iso-8859-1 -*-

#~ PandaStart
#~ Copyright 2015 Michal Kloko All rights reserved.
#~ 
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~ 
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details. 
#~ 
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

""" data/__init__.py

Classes: CoreState(DevCon), CoreEngine(CoreInput, CoreState, CoreGUI)
"""

from panda3d.core import (
    TextNode, TextureStage, TexGenAttrib, Texture, TransparencyAttrib,
    AmbientLight, PointLight
)

from direct.gui.OnscreenText import OnscreenText
from direct.gui.DirectFrame import DirectFrame
from direct.task import Task

import time

from gui import DevCon
from CoreInput import *

# Some shortcuts
camX = base.camera.getX
camY = base.camera.getY
camZ = base.camera.getZ
camH = base.camera.getH
camP = base.camera.getP
camR = base.camera.getR
camPos = ((camX, camY, camZ), (camH, camP, camR))

class CoreState(DevCon):
    """Inherits: none

    Description: All engine vars and active states. Most vars will
    eventually be set via config and/or console. These are defaults."""

    # Set some default paths
    assets = 'assets/'
    models = assets+'models/'
    textures = assets+'textures/'
    skyboxes = textures+'skybox/'

    def __init__(self):
        DevCon.__init__(self)

    ####################################################################
    ### Engine Settings - w_=world,r_=render,i_=input,etc

    # Vars that don't need to be updated in real time
    r_camstart = ((0,0,0),(0,0,0)) # ((pos),(hpr))

    r_anistropic_level = 16 # Will investigate @property later

    r_cam = base.cam.node()

    i_controlstyle = 1

    @property
    def w_ambient_light(self):
        return self.amlight.getColor()
    @w_ambient_light.setter
    def w_ambient_light(self,val):
        self.amlight.setColor(val)
        
    @property
    def w_skytex(self):
        return self.skybox.getTexture()
    @w_skytex.setter
    def w_skytex(self,val):
        self.skyBox.setTexture(loader.loadCubeMap(val))

    @property
    def w_skyboxenabled(self):
        if self.skyBox.isHidden():
            return False
        else:
            return True

    @w_skyboxenabled.setter
    def w_skyboxenabled(self,val):
        if val:
            self.skyBox.show()
        else:
            self.skyBox.hide()

    @property
    def r_camloc(self):
        """Main camera location, like ((x,y,z),(h,p,r)). If set like
        ((x,y,z),None) will change only pos and vice versa"""
        return camPos
    @r_camloc.setter
    def r_camloc(self,val):
        try:
            if type(val[0]) == tuple:
                base.camera.setPos(val[0])
            if type(val[1]) == tuple:
                base.camera.setHpr(val[1])
        except Exception, e:
            print e

    @property # Lens functions may not be correct, but seems to work
    def r_lens(self):
        return self.r_cam.getLens()
    @r_lens.setter
    def r_lens(self,val):
        self.r_cam.setLens(val)

    # Vars that DO need to be updated in real time
    @property
    def r_view_dist(self):
        return self.r_lens.getFar()
    @r_view_dist.setter
    def r_view_dist(self,val):
        self.r_lens.setFar(val)
        self.r_cam.setLens(self.r_lens)

    @property
    def r_fov(self):
        return self.r_lens.getFov()
    @r_fov.setter
    def r_fov(self,val):
        self.r_lens.setFov(val)
        self.r_cam.setLens(self.r_lens)

    @property
    def r_spacecolor(self):
        return base.getBackgroundColor()
    @r_spacecolor.setter
    def r_spacecolor(self,val):
        base.setBackgroundColor(val)

    ####################################################################
    ### Keyboard / Mouse
    i_hpspeed = 0.05 # Heading/pitch speed
    i_rollspeed = 0.05 # Roll speed
    i_basespeed = 0.05 # Maybe not needed

    keyCfg = {
        'Fwd':'w',
        'Rev':'s',
        'SlideL':'a',
        'SlideR':'d',
        'RollR':'e',
        'RollL':'q',
        'TurnL':'arrow_left',
        'TurnR':'arrow_right',
        'PitchUp':'arrow_up',
        'PitchDn':'arrow_down',
        'DecSpd':'[',
        'IncSpd':']',
        'DevCon':'`',
        'Exit':'escape'
    }


    def paused(self, task): return task.cont

    def running(self, task):
        self.handleInput(self.i_controlstyle,task)

        # Less frequently run tasks...
        #~ timeChange = task.time - self.taskDelay[0]
        #~ if timeChange > self.taskDelay[1]:
            #~ if self.onDevHud: self.updateDevHud(task.time)
            #~ # Add additional tasks here
#~ 
            #~ self.taskDelay[0] = task.time

        return task.cont

    def varList(self, withVals = False):
        """Returns all engine variables as list. If withVals, returns a
        dict w/ variables AND their current values"""

        if withVals:
            theList = {}
            for var in CoreState.__dict__:
                # omit python-in-built attributes and
                # getVarVals()+getVars() functions from list.
                if (not var.startswith("__")
                and not var.startswith("getVar")):
                    # return value if updated since run-time
                    if var in self.__dict__:
                        theList[var] = getattr(self, var)
                    else:
                        # else return value of default b/c no change
                        # since run-time...
                        theList[var] = CoreState.__dict__[var]
            return theList
        else:
            return [
                attr for attr in dir(CoreState)
                if not attr.startswith("getVar")
                and not attr.startswith("__")
            ]

    def switchScene(self,scene):
        self.scene.detachNode()
        self.scene = scene.load(self)


class CoreEngine(CoreInput, CoreState):
    """Inherits: uf CoreInput

    Description: The main engine; ties all classes together"""

    def __init__(self):
        """Arguments:
        autoStart (bool) - False for dev purposes
        """
        CoreInput.__init__(self)
        CoreState.__init__(self)

        ################################################################
        ### Skybox
        print "Creating skybox..."
        self.skyBox = loader.loadModel(self.models+'skysphere.egg')
        
        self.skyBox.set_bin("background", 0)
        self.skyBox.set_depth_write(False)
        self.skyBox.set_compass()
        self.skyBox.setScale(20) # Needed?
        self.skyBox.setShaderOff()
        self.skyBox.setLightOff()
        self.skyBox.setP(-90)
        self.skyBox.setTexGen( # Auto texture coords...Needed?
            TextureStage.getDefault(),
            TexGenAttrib.MWorldPosition
        )
        self.skyBox.setTexProjector(
                TextureStage.getDefault(),
                render, self.skyBox
        )
        self.skyBox.reparentTo(base.camera)
        print "Skybox creation done.\n"

        self.amlight = AmbientLight('alight')
        self.alnp = render.attachNewNode(self.amlight)
        render.setLight(self.alnp)
        ################################################################
        ### Engine Setup
        ################################################################
        ### Camera
        print "Setting up camera..."
        r_view_dist = 10000
        self.r_fov = 45
        base.setFrameRateMeter(True) # Set from options or w/e
        base.camera.setPos(self.r_camstart[0])
        base.camera.setHpr(self.r_camstart[1])
        #base.camLens.setNear(0.01)
        print "Camera setup done.\n"

        ################################################################
        ### Counters & Trackers
        self.engineState = self.running
        self.taskDelay = [0, 0.1] # [counter, delay in seconds]

        # Stuff to show on dev hud, e.g. onDevHud['label'] = varToTrack
        self.onDevHud = {}
        #~ self.devHud = OnscreenText(
            #~ text = 'N/A',
            #~ pos = (1.75, -1),
            #~ fg = (1, 1, 1, 1),
            #~ scale = (0.03, 0.03),
            #~ align = TextNode.ALeft,
            #~ mayChange = True
        #~ )

        ################################################################
        ### Start main loop
        taskMgr.add(self.mainLoop, 'MainLoop')

    def mainLoop(self, task):
        return self.engineState(task)

    def stop(self, completely = False):
        """Arguments: bool completely = False

        Description: Shuts down the engine. Completely means exit to
        desktop."""

        taskMgr.remove('MainLoop') # kill the main loop
        #self.flushUniverse()
        #self.flushGalaxies(True)
        if completely == True:
            exit()

    def updateDevHud(self, taskTime):
        self.onDevHud['Cam Pos'] = (camX(), camY(), camZ())
        self.onDevHud['Cam Hpr'] = (camH(), camP(), camR())
        self.onDevHud['Move Speed'] = self.i_basespeed
        self.onDevHud['Win Size'] = (
            base.win.getXSize(), base.win.getYSize()
        )

        text = ''
        for item in self.onDevHud:
            text += '\n'+item+': '+str(self.onDevHud[item])

        self.devHud.setText(text)
