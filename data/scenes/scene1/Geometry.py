# -*- coding: iso-8859-1 -*-
""" data/scenes/Geometry.py

Classes: GeometryData, SurfaceData, CubeData, SphereData, CustomData,
CoreGeom, Cube, Sphere, Surface
"""
#~ PyHeightmap © 2015 Michal Kloko
#~
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details.
#~
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

from panda3d.core import (
    NodePath, GeomVertexFormat, GeomVertexWriter, GeomVertexData, Geom,
    GeomTriangles, GeomNode, GeomPoints
)

import math

def roundWhole(num, base=5): # Do we still need this?
    """Arguments: float/int num, int base = 5

    Description: Round num to nearest whole "base" """

    return int(base * round(float(num) / base))

sand = (1,1,0,1)
rock = (0.5,0.5,0.5,1)
grass = (0,1,0,1)
snow = (1,1,1,1)

class GeometryData(object):
    """Inherits: None

    Description: Standard methods for Geometry Data classes. Useless
    alone and almost useless as a whole..."""

    def getVertex(self, i):
        return self.vertices[i]

    def getNormal(self, i):
        return self.normal[i]

    def getTriangle(self, i):
        return self.triangles[i]

    def getUv(self, i):
        return self.uv[i]

    def getColor(self, i):
        return self.color[i]


class SurfaceData(GeometryData):
    def __init__(self,
        heightmap = None,
        size = 256,
        scale = 1,
        color = (1,1,1,1),
        drawMode = GeomPoints,
        zscale = 0.01
    ):
        self.drawMode = drawMode

        self.triangles = []

        nVerts = size / 2

        if heightmap: # Set Z to height'
            if size % 2 == 0:
                # This if/else keeps the surface centered at origin but
                # there may be (probably is) a better way...
                self.vertices = [
                    (
                        (x+0.5)*scale,
                        (y+0.5)*scale,
                        -heightmap.getHeight(
                            x+nVerts,y+nVerts
                        ) * zscale
                    )

                    for x in xrange(-nVerts,nVerts)
                    for y in xrange(-nVerts,nVerts)
                ]
            else: # Else, we add one to xrange to center
                self.vertices = [
                    (
                        x*scale,
                        y*scale,
                        -heightmap.getHeight(
                            x+nVerts,y+nVerts
                        ) * zscale
                    )

                    for x in xrange(-nVerts,nVerts+1)
                    for y in xrange(-nVerts,nVerts+1)
                ]

        else: # else, render a flat surface
            if size % 2 == 0:
                # see above comment...
                self.vertices = [
                    ((x+0.5)*scale, (y+0.5)*scale,0)

                    for x in xrange(-nVerts,nVerts)
                    for y in xrange(-nVerts,nVerts)
                ]
            else: # Else, we add one to xrange to center
                self.vertices = [
                    (x*scale,y*scale,0)

                    for x in xrange(-nVerts,nVerts+1)
                    for y in xrange(-nVerts,nVerts+1)
                ]


        if color:
            if type(color) == tuple:
                self.color = [color]*len(self.vertices)
            elif type(color) == int:
                self.color = [(color,color,color,1)]*len(self.vertices)
            elif color == 'grayscale':
                self.color = []
                for x in xrange(nVerts*2):
                    for y in xrange(nVerts*2):
                        h = heightmap.hmap.getXel(x,y)[0]
                        self.color.append((h,h,h,1))
            elif 'PNMImage' in str(type(color)):
                self.color = []
                for x in xrange(nVerts*2):
                    for y in xrange(nVerts*2):
                        c = heightmap.getColor(x,y)
                        r,g,b = (c[0],c[1],c[2])
                        self.color.append((r,g,b,1))
        else:
            self.color = None
            self.uv = [(x,y) for x,y,z in self.vertices] # Wrong

        spr =  (size - 1) # squares per row
        t0 = (0,size+1,size)
        t1 = (0,1,size+1)

        for it in xrange(0,spr):
            for it in xrange(0,spr):
                self.triangles.append(t0)
                self.triangles.append(t1)
                t0 = (t0[0]+1,t0[1]+1,t0[2]+1)
                t1 = (t1[0]+1,t1[1]+1,t1[2]+1)
            t0 = (t0[0]+1,t0[1]+1,t0[2]+1)
            t1 = (t1[0]+1,t1[1]+1,t1[2]+1)

        self.normal = self.vertices

        print "Surface triangles:",len(self.triangles)
        print "Surface vertices:",len(self.vertices)


class CubeData(GeometryData):
    triangles = (
        # front
        (0 * 3 + 1, 4 * 3 + 1, 5 * 3 + 1),
        (0 * 3 + 1, 5 * 3 + 1, 1 * 3 + 1),
        # back
        (6 * 3 + 1, 2 * 3 + 1, 3 * 3 + 1),
        (6 * 3 + 1, 3 * 3 + 1, 7 * 3 + 1),
        # left
        (2 * 3 + 0, 0 * 3 + 0, 1 * 3 + 0),
        (2 * 3 + 0, 1 * 3 + 0, 3 * 3 + 0),
        # right
        (4 * 3 + 0, 6 * 3 + 0, 7 * 3 + 0),
        (4 * 3 + 0, 7 * 3 + 0, 5 * 3 + 0),
        # top
        (1 * 3 + 2, 5 * 3 + 2, 7 * 3 + 2),
        (1 * 3 + 2, 7 * 3 + 2, 3 * 3 + 2),
        # bottom
        (2 * 3 + 2, 6 * 3 + 2, 4 * 3 + 2),
        (2 * 3 + 2, 4 * 3 + 2, 0 * 3 + 2)
    )

    uv = (
        [1, 0], [0, 0], [1, 0], # 0
        [0, 0], [0, 1], [1, 1], # 1
        [1, 1], [1, 0], [0, 0], # 2
        [0, 1], [0, 0], [0, 1], # 3
        [0, 0], [1, 0], [1, 1], # 4
        [0, 1], [1, 1], [1, 0], # 5
        [1, 0], [1, 1], [0, 1], # 6
        [1, 1], [0, 1], [0, 0], # 7
    )

    def __init__(self,
        drawMode = GeomTriangles,
        colorArgs = None,
        width = 1,
        height = 1,
        depth = 1
        ):

        self.drawMode = drawMode
        self.vertices = []
        self.normal = []
        self.color = []

        for x in [0, width]:
            for y in [0, height]:
                for z in [0, depth]:
                    self.vertices.append([x, y, z])
                    self.vertices.append([x, y, z])
                    self.vertices.append([x, y, z])
                    nx = (x - width / 2) / (width / 2)
                    self.normal.append([nx, 0, 0])
                    ny = (y - height / 2) / (height / 2)
                    self.normal.append([0, ny, 0])
                    nz = (z - depth / 2) / (depth / 2)
                    self.normal.append([0, 0, nz])
                    if colorArgs:
                        self.color.append(
                            Color(*colorArgs, noisePos = (x, y, z))
                        )
                        self.color.append(
                            Color(*colorArgs, noisePos = (x, y, z))
                        )
                        self.color.append(
                            Color(*colorArgs, noisePos = (x, y, z))
                        )


class SphereData(GeometryData):
    def __init__(self,
        drawMode = GeomTriangles,
        colorArgs = None,
        radius = 1,
        widthSegments = 32,
        heightSegments = 17,
        angle = 360,
        inverted = False,
        ):

        assert(heightSegments >= 3)
        assert(widthSegments >= 4)
        self.drawMode = drawMode
        self.radius = radius

        self.vertices = []
        self.uv = []
        self.color = []

        for j in xrange(heightSegments - 1):
            for i in xrange(widthSegments):
                thetaS = float(j) / (heightSegments - 2)
                theta = thetaS * math.pi * (angle / 360.)
                phiS = float(i) / (widthSegments - 1)
                phi = phiS * math.pi * 2
                x = math.sin(theta) * math.cos(phi)
                y = math.cos(theta)
                z = -math.sin(theta) * math.sin(phi)

                self.vertices.append((x, z, y))

                if not colorArgs: self.uv.append((thetaS, phiS))
                else: self.color.append(Color(*colorArgs))

        if 'Tri' in str(self.drawMode):
            self.triangles = []
            for j in xrange(heightSegments - 2):
                for i in xrange(widthSegments - 1):
                    v0 = j * widthSegments + i
                    v1 = (j + 1) * widthSegments + i + 1
                    v2 = j * widthSegments + i + 1

                    if not inverted:
                        self.triangles.append((v2, v1, v0))
                    else:
                        self.triangles.append((v0, v1, v2))

                    v0 = j * widthSegments + i
                    v1 = (j + 1) * widthSegments + i
                    v2 = (j + 1) * widthSegments + i + 1
                    if not inverted:
                        self.triangles.append((v2, v1, v0))
                    else:
                        self.triangles.append((v0, v1, v2))

    def getVertex(self, i):
        return [
            self.vertices[i][0] * self.radius,
            self.vertices[i][1] * self.radius,
            self.vertices[i][2] * self.radius
        ]

    def getNormal(self, i):
        return self.vertices[i]


class CustomData(GeometryData):
    def __init__(self,
        vertices = None,
        drawMode = GeomPoints,
        colors = None,
        triangles = None):

        self.drawMode = drawMode
        self.vertices = vertices
        self.color = colors
        self.triangles = triangles


class CoreGeom(NodePath):
    def __init__(self, nodeName = 'Geometry'):
        NodePath.__init__(self, nodeName)
        self.nodeName = nodeName

    def addGeometry(self, geomData):
        vFormat = GeomVertexFormat.getV3n3c4t2()
        vData = GeomVertexData(
            self.nodeName+'-vData', vFormat, Geom.UHStatic
        )
        vWriter = GeomVertexWriter(vData, 'vertex')
        nWriter = GeomVertexWriter(vData, 'normal')

        if not geomData.color:
            tWriter = GeomVertexWriter(vData, 'texcoord')
        else:
            cWriter = GeomVertexWriter(vData, 'color')

        prim = geomData.drawMode(Geom.UHStatic)

        if 'Tri' in str(geomData.drawMode):
            vtxTargetId0 = vtxTargetId1 = vtxTargetId2 = None
            vtxDataCounter = 0
            for vs0, vs1, vs2 in geomData.triangles:
                v0 = geomData.getVertex(vs0)
                v1 = geomData.getVertex(vs1)
                v2 = geomData.getVertex(vs2)

                if not geomData.color:
                    uvx0, uvy0 = uv0 = geomData.getUv(vs0)
                    uvx1, uvy1 = uv1 = geomData.getUv(vs1)
                    uvx2, uvy2 = uv2 = geomData.getUv(vs2)

                    # make it wrap nicely
                    if min(uvx0, uvx1, uvx2) < 0.25 \
                    and max(uvx0, uvx1, uvx2) > 0.75:
                        if uvx0 < 0.25: uvx0 += 1
                        if uvx1 < 0.25: uvx1 += 1
                        if uvx2 < 0.25: uvx2 += 1
                else:
                    c0 = geomData.getColor(vs0)
                    c1 = geomData.getColor(vs1)
                    c2 = geomData.getColor(vs2)

                n0 = geomData.getNormal(vs0)
                n1 = geomData.getNormal(vs1)
                n2 = geomData.getNormal(vs2)

                vWriter.addData3f(*v0)
                nWriter.addData3f(*n0)
                if not geomData.color: tWriter.addData2f(*uv0)
                else: cWriter.addData4f(*c0)
                vtxTargetId0 = vtxDataCounter
                vtxDataCounter += 1

                vWriter.addData3f(*v1)
                nWriter.addData3f(*n1)
                if not geomData.color: tWriter.addData2f(*uv1)
                else: cWriter.addData4f(*c1)
                vtxTargetId1 = vtxDataCounter
                vtxDataCounter += 1

                vWriter.addData3f(*v2)
                nWriter.addData3f(*n2)
                if not geomData.color: tWriter.addData2f(*uv2)
                else: cWriter.addData4f(*c2)
                vtxTargetId2 = vtxDataCounter
                vtxDataCounter += 1

                prim.addVertex(vtxTargetId0)
                prim.addVertex(vtxTargetId1)
                prim.addVertex(vtxTargetId2)
        else:
            row = 0
            for v in geomData.vertices:
                vWriter.addData3f(*v)
                cWriter.addData4f(*geomData.color[row])
                row += 1
            prim.addConsecutiveVertices(0, len(geomData.vertices))

        prim.closePrimitive()

        geom = Geom(vData)
        geom.addPrimitive(prim)

        node = GeomNode(self.nodeName+'-gNode')
        node.addGeom(geom)

        nodePath = self.attachNewNode(node)

        return nodePath


class Cube(CoreGeom):
    def __init__(self,
        nodeName,
        drawMode = GeomTriangles,
        colorArgs = None,
        height = 2,
        width = 2,
        depth = 2
        ):

        CoreGeom.__init__(self, nodeName)

        self.addGeometry(
            CubeData(drawMode, colorArgs, width, height, depth)
        )


class Sphere(CoreGeom):
    def __init__(self,
        nodeName,
        drawMode = GeomTriangles,
        colorArgs = None,
        radius = 0.5,
        segments = 16,
        angle = 360,
        inverted = False
        ):

        CoreGeom.__init__(self, nodeName)

        self.radius = radius
        sphere = self.addGeometry(
            SphereData(
                drawMode = drawMode,
                colorArgs = colorArgs,
                radius = radius,
                widthSegments = segments,
                heightSegments = segments,
                angle = angle,
                inverted = inverted
            )
        )


class Surface(CoreGeom):
    def __init__(self,
        nodeName,
        heightmap = None,
        size = 256,
        scale = 0.1,
        color = (0,1,0,1),
        drawMode = GeomTriangles,
        zscale = 0.01
        ):

        CoreGeom.__init__(self, nodeName)

        surface = self.addGeometry(
            SurfaceData(
                heightmap = heightmap,
                size = size,
                scale = scale,
                color = color,
                drawMode = drawMode,
                zscale = zscale
            )
        )
