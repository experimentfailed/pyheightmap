# -*- coding: iso-8859-1 -*-
""" data/scenes/scene1/__init__.py

Classes: None
"""
#~ PyHeightmap © 2015 Michal Kloko
#~
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details.
#~
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

from panda3d.core import (
    TextureStage, TexGenAttrib, Texture, TransparencyAttrib,
    AmbientLight, PointLight, NodePath, DirectionalLight
)
from direct.gui.DirectGui import OnscreenImage

from HeightmapGen import *
from Geometry import *

def load(engine):
    ################################################################
    ### Default Heightmap - Maybe we'll start w/ a flat surface?
    engine.r_camloc = ((0,-3900,3200),(0,-40,0))
    scene = NodePath("MainScene")

    engine.w_skytex = engine.skyboxes+'skybox01/sb_#.png'
    engine.skyBox.setH(-90) # Rotate so sun comes from dlight

    print "Generating default heightmap..."
    heightmap = Heightmap(size=256)
    heightmap.gen_base_hmap(
        add_fbm = True,
        amp = 300,
        lacunarity = 2,
        octaves = 8,
        gain = 0.5,
        seed = engine.w_seed,
        nsx=200,
        nsy=200,
        nsz=200
    )
    heightmap.gen_base_cmap(zebraRamp())
    #~ heightmap.thermal_erode(
        #~ iterations = 8, thresh = 0, amt = 16, rev = True
    #~ )
    heightmap.thermal_erode(
        iterations = 8, thresh = 0, amt = 2, method=neumannr
    )
    #heightmap.save_file("test1.bmp")
    print "Default heightmap generation done.\n"

    ################################################################
    ### Terrain Surface
    print "Generating terrain surface..."
    engine.terrain = Surface(
        'Terrain',
        heightmap,
        drawMode=GeomTriangles,
        size=256,
        scale=10,
        zscale=1,
        color=heightmap.cmap
    )
    #~ engine.w_surfacetex = engine.textures+'grass/grass1.jpg'
    #~ engine.w_surfacetex.setMagfilter(Texture.FTLinear)
    #~ engine.w_surfacetex.setMinfilter(Texture.FTLinearMipmapLinear)
    #~ engine.w_surfacetex.setAnisotropicDegree(engine.r_anistropic_level)
    #engine.terrain.setShaderAuto()
    engine.terrain.setHpr(-180,0,180) # Need to fix in CoreGeom
    engine.terrain.setPos(0,0,0)
    #~ engine.terrain.setTexScale(TextureStage.getDefault(),0.125,0.125)
    engine.terrain.reparentTo(scene)
    print "Terrain surface generation done.\n"

    ################################################################
    ### Water
    print "Generating water plane..."
    #water = Surface('Water',color=(0,0,1,0.001), size=8,scale=368)
    engine.water = Surface(
        'Water',color = None, size=8,scale=364 # Needs to uto scale
    )
    engine.w_watertex = engine.textures+"water/water01/frame1.png"
    engine.w_watertex.setMagfilter(Texture.FTLinear)
    engine.w_watertex.setMinfilter(Texture.FTLinearMipmapLinear)
    engine.w_watertex.setAnisotropicDegree(16)
    engine.w_waterlevel = 85
    engine.water.setR(180) # again: need to fix in CoreGeom
    engine.water.setTransparency(TransparencyAttrib.MAlpha)
    engine.r_watertansparency = 0.8
    engine.water.setTexScale(
        TextureStage.getDefault(),0.125,0.125
    )
    engine.water.setTwoSided(True)
    engine.water.reparentTo(scene)
    print "Water plane generation done.\n"

    ################################################################
    ### Lights - All wrong at the moment
    print "Setting up lights..."
    alc = 0.7
    engine.w_ambient_light = (alc,alc,alc,1)

    dlc = 0.001 # dlight color
    engine.dlight = DirectionalLight('dlight')
    engine.dlight.setColor((dlc,dlc,dlc, 1))
    engine.dlnp = scene.attachNewNode(engine.dlight)
    engine.dlnp.setZ(1)
    engine.dlnp.lookAt(engine.terrain)
    scene.setLight(engine.dlnp)

    print "Light setup done.\n"

    engine.cPrev2dTex = Texture()
    engine.cPrev2dTex.load(heightmap.cmap)
    engine.cPrev2d = OnscreenImage(
        parent=base.a2dTopRight,
        image=engine.cPrev2dTex,
        pos=(-0.3,0,-0.38),
        scale=0.25
    )
    engine.cPrev2d.setBin('fixed', -50)

    engine.hPrev2dTex = Texture()
    engine.hPrev2dTex.load(heightmap.hmap)
    engine.hPrev2d = OnscreenImage(
        parent=base.a2dTopRight,
        image=engine.hPrev2dTex,
        pos=(-0.3,0,-0.98),
        scale=0.25
    )
    engine.hPrev2d.setBin('fixed', -50)

    scene.reparentTo(render)

    return scene
