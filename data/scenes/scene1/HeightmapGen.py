# -*- coding: iso-8859-1 -*-
""" data/scenes/HeightmapGen.py

Classes: Heightmap
"""
#~ PyHeightmap © 2015 Michal Kloko
#~
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details.
#~
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

from panda3d.core import (
    PerlinNoise3, PerlinNoise2, StackedPerlinNoise3, PNMImage
)

import math

def moore(x,y,distance=1):
    return [
        (x-distance, y-distance),
        (x, y-distance),
        (x+distance, y-distance),
        (x-distance, y),
        (x+distance, y),
        (x-distance, y+distance),
        (x, y+distance),
        (x+distance, y+distance)
    ]


def neumann(x,y,distance=1):
    return [
        (x-distance,y),
        (x+distance,y),
        (x,y-distance),
        (x,y+distance)
    ]


def neumannr(x,y,distance=1):
    return [
        (x-distance,y-distance),
        (x+distance,y+distance),
        (x+distance,y-distance),
        (x-distance,y+distance)
    ]


### Ramp functions adpoted from Mudpie project:
# https://code.google.com/p/mudpie/
def geoRamp(waterLvl=90,snowLvl=228,grassLvl=(90,128),
treeLvl=(128,228)):
    # To-do: add sand and maybe mud levels
    # Also, needs to consider slope to cover steep surfaces in rock
    result = []
    for i in xrange(waterLvl):
        result.append((92, 92, i + 128))
    for i in xrange(grassLvl[0],grassLvl[1]):
        result.append((64, i, 64))
    for i in xrange(treeLvl[0],treeLvl[1]):
        result.append((i,i,0))
    for i in xrange(snowLvl, 256):
        v = 128 + i / 2
        result.append((v,v,v))
    return result

def thermRamp():
    """Shows much contrast between values"""
    result = []
    for i in xrange(64):
        result.append((0,0,i * 4))
    for i in xrange(64, 128):
        result.append((0, (i-64) * 4, 0))
    for i in xrange(128, 192):
        result.append(((i-128) * 4,0,0))
    for i in xrange(192, 256):
        result.append(((i-192)*4,(i-192)*4,0))
    return result

def landSeaRamp(sealevel=0.5):
    if type(sealevel) == type(1.0):
        sealevel = int(sealevel*256)
    c2 = sealevel/2
    t = 256 - sealevel
    result = []
    for i in xrange(c2):
        result.append((0, 0, 255 * i / c2))
    for i in xrange(c2, sealevel):
        result.append((0, 255 * (i-c2) / c2, 255))
    for i in xrange(t):
        result.append((i * 255 / t, 128 + i * 128 / t, 0))
    return result

def fireRamp():
    result = []
    for i in xrange(128):
        result.append((i+64,0,0))
    for i in xrange(128, 256):
        result.append((i/2+128,(i-128)*3/2,0))
    return result

def zebraRamp():
    result = []
    for i in range(256):
        x = math.sin(i/16.0)
        x = math.floor((1.0-(x**2)) * i)
        result.append((x,i,x))
    return result

def fbm(x,y,seed,z=0,octaves=8,lacunarity=2,gain=0.5,table_size=256,
nsx=10,nsy=10,nsz=10):
    """
    Fractal Brownian Motion. Try help(Heightmap.gen_base_hmap) or
    so...
    """
    noise = PerlinNoise3(nsx, nsy, nsz, table_size, seed)
    amp = 1
    freq = 1
    total = 0
    for i in xrange(octaves):
        total += amp * noise(x * freq, y * freq, z * freq)
        amp *= gain
        freq *= lacunarity
    return total


class Heightmap(object):
    def __init__(self,size = 256,name="heightmap"):
        """
        Arguments:
        size (int) - x/y pixel size of image. Eventually will support
        rectangles.

        name (str) - Unique name for image
        """
        self.hmap = PNMImage(size,size)
        self.cmap = PNMImage(size,size)
        self.name = name
        self.size = size

    # Heightmap
    def getHeight(self,x,y):
        """
        Returns int of height from 0-255 at given x,y coordinates.
        """
        return self.hmap.getXelVal(x,y)[0]
    def setHeight(self,x,y,val):
        """
        Sets int as height from 0-255 at given x,y coordinates.
        """
        self.hmap.setXelVal(x,y,val)

    # Colormap
    def getColor(self,x,y):
        """
        Returns tuple of rgb floats at given x,y coordinates. Notice:
        unlike set/getHeight functions, get/setColor functions get/set
        different values. This is because we want to generally manage
        setting of colors in traditional 0-255 RGB colorspace, while
        when getting colors we want Panda3d-friendly decimal RGB values.

        More on that later...
        """
        return self.cmap.getXel(x,y)
    def setColor(self,x,y,val):
        """
        Sets tuple of rgb ints at given x,y coordinates.
        """
        self.cmap.setXelVal(x,y,val)

    def save_file(self,name):
        """
        Save heightmap to png file. No need to pass file extension.
        NOTE: Function is untested.

        Arguments:
        name (str) - File name without extension.
        """
        self.hmap.write(name or self.name+".png")

    def surrpx(self,x,y,method=moore,radius=1):
        """
        Returns x,y coords of surrounding pixels, based on method.

        Arguments:
        x,y (int) - Origin pixel
        method (name) - One of 'moore', 'neumann', or 'neumannr'
        radius (int) - Distance from origin to get surrounding pixels.
        """
        # Radius not implemented yet
        return [
            p for p in method(x,y)
            if (p[0] >= 0) # Constrain to image size
            and (p[1] >= 0)
            and (p[0] < self.hmap.getXSize())
            and (p[1] < self.hmap.getYSize())
        ]

    def gen_base_hmap(self,seed,add_fbm=1,amp=375,offset=0,octaves=8,
    lacunarity=2,gain=0.5,nsx=250,nsy=250,nsz=250):
        """
        Generates an initial heightmap, with or without Fractal
        Brownian Motion.

        Arguments:
        seed (int) - Seed to use for Perlin Noise

        add_fbm (bool) - Use Fractal Brownian Motion (fbm() function)
        or not.

        amp (int) - Amplifies the noise value (increases z-component,
        a.k.a. height). Too high will cause "clipping" (values > 255),
        but is a quick/dirty way to get nice plateaus. Too low will
        cause little to no detail (all or mostly black image).

        offset (float) - Moves x,y position in the noise. Effectively
        similar to changing the seed.

        octaves (int) - Number of octaves to use, if using fbm.

        lacunarity (float) - Lacunarity value for fbm. higher = more
        jagged terrain. Lower = smoother.

        gain (float) - Gain setting for fbm.

        nsx,nsy,nsz (float) - Noise scale. Lower values will yield
        "noisier" noise (perhaps akin to river deltas, island clusters,
        or utter chaos), while higher values will yield "quieter" noise
        (like planes with curvy rivers or coast lines).
        """
        print "Generating base heightmap..."
        clipcnt = 0
        for x in xrange(self.size):
            for y in xrange(self.size):
                a,b = ((x+offset), (y+offset))
                if add_fbm:
                    n = abs(int(
                            fbm(
                                x=a,y=b,z=offset,
                                nsx=nsx,
                                nsy=nsy,
                                nsz=nsz,
                                seed=seed,
                                octaves=octaves,
                                lacunarity=lacunarity,
                                gain=gain
                            ) * amp
                        ))
                else:
                    noise = PerlinNoise2(nsx, nsy, 256, seed)
                    n = abs(int(noise(x,y) * amp))

                if n > 255:
                    n = 255
                    clipcnt += 1

                self.setHeight(x,y,n)

        if clipcnt: print clipcnt, (
            "pixels clipped! Check your amp argument for"
            "Heightmap.gen_base_map(), if this was not intentional!"
        )
        print "Minimum height:", self.get_min_val()
        print "Maximum height:", self.get_max_val()
        print "Base heightmap generation done."

    def gen_base_cmap(self,ramp=geoRamp()):
        for x in xrange(self.size):
            for y in xrange(self.size):
                self.setColor(x,y,ramp[self.getHeight(x,y)])

    def get_max_val(self):
        """
        Returns the highest height value in the heightmap
        """
        lastmax = 0
        for x in xrange(self.hmap.getXSize()):
            for y in xrange(self.hmap.getYSize()):
                px = self.getHeight(x,y)
                if px > lastmax:
                    lastmax = px
        return lastmax


    def get_min_val(self):
        """
        Returns the lowest height value in the heightmap
        """
        lastmin = 0
        for x in xrange(self.hmap.getXSize()):
            for y in xrange(self.hmap.getYSize()):
                px = self.getHeight(x,y)
                if px < lastmin:
                    lastmin = px
        return lastmin


    def thermal_erode(self,method=moore,thresh=0,amt=2,iterations = 8,
    rev = False):
        """
        Thermal erosion sculpts landscapes with gravity. This is sort of
        like a smoothing function. It looks at each pixel's surrounding
        pixels, then subtracts the heights (for each surrounding pixel,
        subtract origin height). If the difference is greater than the
        threshold (in normal/non-reversed mode), it will take some
        amount (argument: amt) from the origin pixel, and apply it to
        the surrounding pixels. Reverse mode is identical, except
        applies when the height difference between the origin and
        surrounding pixels is less than the threshold.

        Arguments:
        method (name) - Pattern to use for each pixel's surrounding
        pixels. Can be (without quotes): "moore", "neumann", or
        "neumannr" (neumann rotated).

        thresh (int) - Threshold. When the value (color) difference
        between the origin pixel and the current neighbor is above
        (normal/non-reversed mode) or below (reversed mode) this value,
        then apply erosion amount (argument amt).

        amt (int) - Amount to subtract from the origin and add to the
        neighboring pixels.

        iterations (int) - How many times to go over the heightmap, and
        apply the erosion effect.
        """
        print "Applying thermal erosion..."
        ecnt = 0
        mindiff = 0 # Tracks max diff; useful for deciding on threshold
        maxdiff = 0 # "      min "
        iter_cnt = 1
        for i in xrange(iterations):
            print "    Iteration number:", iter_cnt
            for x in xrange(self.hmap.getXSize()):
                for y in xrange(self.hmap.getYSize()):
                    neighbors = self.surrpx(x,y,method)
                    for px in neighbors:
                        nv = self.getHeight(px[0],px[1]) # Neighbor val
                        ov = self.getHeight(x,y) # Origin val
                        pixdiff = ov - nv
                        if pixdiff < mindiff: mindiff = pixdiff
                        if pixdiff > maxdiff: maxdiff = pixdiff
                        if not rev:
                            if (pixdiff >= thresh):
                                nv += amt
                                ov -= amt
                                nv,ov = (abs(nv),abs(ov))
                                self.setHeight(px[0],px[1], nv)
                                self.setHeight(x,y, ov)
                                ecnt += 1
                        else:
                            if (pixdiff <= thresh):
                                nv += -amt
                                ov -= -amt
                                nv,ov = (abs(nv),abs(ov))
                                self.setHeight(px[0],px[1], nv)
                                self.setHeight(x,y, ov)
                                ecnt += 1
            iter_cnt += 1

        print "Total pixels erroded:", ecnt
        print "Minimum pixel difference:", mindiff
        print "Maximum pixel difference:", maxdiff
        print "Min height:", self.get_min_val()
        print "Max height:", self.get_max_val()
        print "Thermal erosion done.\n"


    def hydro_erode(self,rain_amt = 50,iterations = 8,solubility = 4,
    evaporation = 4,capacity = 4):
        """ NOT IMPLEMENTED """
        rainmap = [
            abs(int(perlin2(x*2,y*2) * rain_amt))
            for x in xrange(self.hmap.getXSize())
            for y in xrange(self.hmap.getYSize())
        ]

        print max(rainmap), min(rainmap)

        return rainmap
