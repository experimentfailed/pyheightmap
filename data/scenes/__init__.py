# -*- coding: iso-8859-1 -*-

#~ PandaStart
#~ Copyright 2015 Michal Kloko All rights reserved.
#~ 
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~ 
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details. 
#~ 
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

""" data/__init__.py

Classes: CoreState(DevCon), CoreEngine(CoreInput, CoreState, CoreGUI)
"""

from panda3d.core import NodePath

from os import walk

# __all__ = ['scene1','scene2']

# I feel like there is a better way to do this...
__all__ = [
    p[0].split("\\")[3] for p in walk('.\data\scenes')

    if p[0].strip(".\\data") != 'scenes'
]
