# -*- coding: iso-8859-1 -*-

#~ PandaStart
#~ Copyright 2015 Michal Kloko All rights reserved.
#~ 
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~ 
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details. 
#~ 
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

""" data/gui/__init__.py

Classes: DevCon, CoreGUI
"""

from StringIO import StringIO
import sys

from Toolbars import *
from Menus import *


class DevCon(object):
    def __init__(self):
        self.dcFont = loader.loadFont(
            'assets/gui/fonts/AverageMono/AverageMono.ttf'
        )
        ################################################################
        ### Dev Console
        self.lineHeight = 0.0483333338052
        self.scrollBgColor = (0,0.5,0.9,1)
        self.scrollThumbColor = (0,0.5,1,1)
        self.scrollBtnColor = (0,0.6,1,1)

        self.devCon = DirectScrolledFrame(
            parent=base.a2dTopCenter,
            frameSize=(-1.5,1.5,-1,0),
            frameColor=(0,0.5,0.8,0.8),
            canvasSize=(-1,1,0.001,2),
            manageScrollBars=False,

            # resize the scrollbar according to window size
            verticalScroll_resizeThumb=True,
            horizontalScroll_resizeThumb=True,

            #~ horizontalScroll_incButton_frameTexture=self.scrollBtnTex,
            #~ horizontalScroll_decButton_frameTexture=self.scrollBtnTex,

            # make all flat, so the texture is as we want it
            verticalScroll_relief=DGG.FLAT,
            verticalScroll_thumb_relief=DGG.FLAT,
            verticalScroll_decButton_relief=DGG.FLAT,
            verticalScroll_incButton_relief=DGG.FLAT,
            horizontalScroll_relief=DGG.FLAT,
            horizontalScroll_thumb_relief=DGG.FLAT,
            horizontalScroll_decButton_relief=DGG.FLAT,
            horizontalScroll_incButton_relief=DGG.FLAT,

            # colors
            verticalScroll_frameColor=self.scrollBgColor,
            verticalScroll_incButton_frameColor=self.scrollBtnColor,
            verticalScroll_decButton_frameColor=self.scrollBtnColor,
            verticalScroll_thumb_frameColor=self.scrollThumbColor,

            horizontalScroll_frameColor=self.scrollBgColor,
            horizontalScroll_incButton_frameColor=self.scrollBtnColor,
            horizontalScroll_decButton_frameColor=self.scrollBtnColor,
            horizontalScroll_thumb_frameColor=self.scrollThumbColor
        )

        self.dcCanvas = self.devCon.getCanvas()
        self.devCon.verticalScroll.setValue(1)
        self.devCon.hide()
        self.devCon.setDepthTest(False)
        self.devCon.setDepthWrite(False)
        self.devCon.setBin('fixed', 50)

        self.dcInput = DirectEntry(
            parent=self.devCon,
            focus=False,
            command=self.devConCmd,
            numLines=1,
            width=59.8,
            scale=0.05,
            pos=(-1.495,0,-1.055),
            frameColor=(0,0.5,0.8,0.9),
            entryFont=self.dcFont
        )

        self.dcOutput = [
            DirectLabel(
                parent=self.dcCanvas,
                text = "Type help or ? for a list of valid commands.",
                text_scale=0.05,
                pos=(-1,0,0.03),
                frameColor=(0,0,0,0),
                text_font=self.dcFont,
                text_align=TextNode.ALeft
            )
        ]

    @property
    def dcVisible(self):
        return self.devCon.isHidden()

    @dcVisible.setter
    def dcVisible(self,val):
        if val == False:
            self.engineState = self.running
            self.devCon.hide()
            self.dcInput['focus'] = False
        else:
            currstr = self.dcInput.get()
            newstr = currstr.replace('`','')
            self.engineState = self.paused
            self.devCon.show()
            self.dcInput.set(newstr)
            self.dcInput['focus'] = True

    def toggleDevCon(self):
            self.dcVisible ^= False

    def devConCmd(self,cmd):
        """Handles Dev Con Input"""
        if cmd in ("cls", "clear"):
            self.dcHistory = ""
        elif cmd in ("help", "?"):
            cmd += "%s\nNot implemented :(" % cmd
        else:
            try:
                # From https://wrongsideofmemphis.wordpress.com/2010/03/01/store-standard-output-on-a-variable-in-python/
                old_stdout = sys.stdout
                result = StringIO()
                sys.stdout = result

                exec cmd

                sys.stdout = old_stdout
                result_string = result.getvalue()

                self.dcPrint("\n%s\n%s" % (cmd,result_string))
            except Exception, e:
                print e
                self.dcPrint("\n%s\n%s" % (cmd,e))

        self.dcInput['focus'] = True

    def dcPrint(self,output):
        """
        Manages Dev Console output
        TO-DO:
        - Truncate output
        - Remove extra line spacing....currently using a lazy approach
        """
        newLines = output.split('\n')[1:]

        def moveLinesUp():
            for i in self.dcOutput:
                i.setZ(i.getZ()+(self.lineHeight*len(newLines)))

        for l in newLines:
            moveLinesUp()

            self.dcOutput.append(
                DirectLabel(
                    parent=self.dcCanvas,
                    text = l,
                    text_scale=0.05,
                    pos=(-1,0,0.03),
                    frameColor=(0,0,0,0),
                    text_font=self.dcFont,
                    text_align=TextNode.ALeft
                )
            )


class CoreGUI(object):
    def __init__(self):
        ################################################################
        ### 2D Preview
        # TO-DO: make draggable/resizeable; add "view full size"
        # Eventually will also have full 2d editing capability, like
        # MS Paint or whatever
        self.guiFont = loader.loadFont(
            'assets/fonts/AverageMono/AverageMono.ttf'
        )

        self.prev2dTex = Texture()
        self.prev2dTex.load(self.heightmap.img)
        self.prev2d = OnscreenImage(
            parent=base.a2dTopRight,
            image=self.prev2dTex,
            pos=(-0.3,0,-0.38),
            scale=0.25
        )
        self.prev2d.setBin('fixed', -50)
        DevCon.__init__(self)

        MainCP.__init__(self)

        Menus.__init__(self)
