# -*- coding: iso-8859-1 -*-

#~ PandaStart
#~ Copyright 2015 Michal Kloko All rights reserved.
#~ 
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~ 
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details. 
#~ 
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

""" data/gui/Menus.py

Classes: MenuItem, Menus
"""

from direct.gui.DirectGui import *
from panda3d.core import (
    Texture, NodePath, Camera, OrthographicLens, TextNode
)

class MenuItem(object):
    def __init__(self,parent,items,pos,name="Test",bgColor=(0,0,0,0)):
        self.button = DirectButton(
            text=name,
            parent=parent,
            relief=DGG.FLAT,
            scale=0.07,
            command=self.toggleMenu,
            pos=pos,
            frameColor=bgColor
        )

        self.frame = DirectFrame(
            parent=self.button,
            frameSize=(-2,2,-1*len(items),1),
            frameColor=(0,0.5,0.8,0.9),
            pos=(1.2,0,-1.2)
        )
        self.frame.hide()

        self.items = []
        z = 0
        lineHeight=1.2
        for i in items:
            self.items.append(
                DirectButton(
                    parent=self.frame,
                    text=i[0],
                    relief=DGG.FLAT,
                    pos=(0,0,z),
                    frameSize=(-2,2,-0.35,0.9)
                )
            )
            z -= lineHeight

    def toggleMenu(self):
        if self.frame.isHidden():
            self.frame.show()
        else:
            self.frame.hide()


class Menus(object):
    # This whole system needs to be more object-orientedified
    def __init__(self):
        self.fileItems = ( # File drop-down
            ('Open',None), # ('Name',command)
            ('Save',None),
            ('Save As',None),
            ('Import',None),
            ('Exit',None)
        )
        self.editItems = ( # Edit drop-down
            ('Undo',None),
            ('Re-do',None),
            ('Options',None)
        )

        self.menuBar = DirectFrame(
            parent=base.a2dTopLeft,
            frameSize=(-2,2,0,0.075),
            frameColor=(0,0.5,0.8,0.9),
            pos=(2,0,-0.075)
        )

        z = 0.016
        self.fileMenu = MenuItem(
            parent=self.menuBar,
            pos=(-1.94,0,z),
            items=self.fileItems,
            name="File",
        )
        self.editMenu = MenuItem(
            parent=self.menuBar,
            pos=(-1.8,0,z),
            items=self.editItems,
            name="Edit",
        )
