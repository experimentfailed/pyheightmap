# -*- coding: iso-8859-1 -*-

#~ PandaStart
#~ Copyright 2015 Michal Kloko All rights reserved.
#~ 
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~ 
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details. 
#~ 
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

""" data/gui/Toolbars.py

Classes: TbItem, Sep, Toolbar
"""

from direct.gui.DirectGui import *
from panda3d.core import (
    Texture, NodePath, Camera, OrthographicLens, TextNode
)

tbHorizontal = 'horizontal'
tbVertical = 'vertical'

class TbItem(DirectFrame):
    """
    Toolbar item
    """
    def __init__(self,
            parent, # A toolbar object
            padding=(0,0,0,0),
            texture=None,
            label='Test',
            orientation=tbVertical,
            proportions=(50,50),
            bgColor=(0,0,0,0)
        ):
        self.parent = parent
        # Rest probably need to be @property

        DirectFrame.__init__(self,
            parent=parent,
            frameSize=size, # Size maybe size minus padding
            frameColor=bgColor,
        )
        self.initialiseoptions(TbItem)


class Sep(DirectFrame):
    """
    A quick and dirty separator object...

    Padding can be tuple of (left,right,bottom,top), or int to set same
    padding for all sides.
    """
    def __init__(self,
        parent,
        padding=0.1,
        color=(1,1,1,1),
    ):
        
        DirectFrame.__init__(self,
            parent=parent,
            frameSize=size, # Size maybe size minus padding
            frameColor=bgColor,
        )
        self.initialiseoptions(TbItem)


class Toolbar(DirectFrame):
    def __init__(self,width,height,
        orientation=tbVertical,
        items={},
        parent=base.a2dLeftCenter,
        color=(1,1,1,1),
        texture=None
        ):

        self.width = width
        self.height = height
        self.orientation = orientation
        self.items = items # A list/tuple of TbItem objects

        size = (-width/2,width/2,-height/2,height/2)

        DirectFrame.__init__(self,
            parent=parent,
            frameSize=size, # Size maybe size minus padding
            frameColor=color,
        )
        self.initialiseoptions(TbItem)
