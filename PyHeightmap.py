# -*- coding: iso-8859-1 -*-

"""
PyHeightmap
Copyright 2015 Michal Kloko All rights reserved.

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that
it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see http://www.gnu.org/licenses/.
"""

from panda3d.core import loadPrcFileData,loadPrcFile

loadPrcFileData('', 'window-title PyHeightmap')
loadPrcFile("./config.cfg")

import direct.directbase.DirectStart

from data import CoreEngine
from data.scenes import *

class App(CoreEngine):
    def __init__(self):
        CoreEngine.__init__(self)

        self.scene = scene1.load(self)

    # Additional engine vars; w_ = world
    w_seed = 987654

    @property
    def w_surfacetex(self):
        return self.terrain.getTexture()
    @w_surfacetex.setter
    def w_surfacetex(self,val):
        self.terrain.setTexture(loader.loadTexture(val))

    @property
    def w_watertex(self):
        return self.water.getTexture()
    @w_watertex.setter
    def w_watertex(self,val):
        self.water.setTexture(loader.loadTexture(val))

    @property
    def w_waterlevel(self):
        return self.water.getZ()
    @w_waterlevel.setter
    def w_waterlevel(self,val):
        self.water.setZ(val)

    @property
    def w_watertansparency(self):
        return self.water.getAlphaScale()
    @w_watertansparency.setter
    def w_watertansparency(self,val):
        self.water.setAlphaScale(val)

app = App()

base.run()
